package com.fertiletech.domestichelp.client.gui;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DatePicker;

public class ComplaintForm extends Composite {
	
	@UiField
	ListBox applicantList;
	
	@UiField
	ListBox issueType;
	
	@UiField
	Label setDate;

	private static ComplaintFormUiBinder uiBinder = GWT
			.create(ComplaintFormUiBinder.class);

	interface ComplaintFormUiBinder extends UiBinder<Widget, ComplaintForm> {
	}

	public ComplaintForm() {
		initWidget(uiBinder.createAndBindUi(this));
		
		applicantList.addItem("Wale Bakare");
		applicantList.addItem("Segun Sobluo");
		applicantList.addItem("Chikwendu Abakporo");
		applicantList.addItem("Riliwan Sobulo");
		applicantList.addItem("Ayodeji Pedro");
		
		issueType.addItem("Theft");
		issueType.addItem("Property Damage");
		issueType.addItem("Pregnancy");
		issueType.addItem("Theft");
		issueType.addItem("Fighting");
		issueType.setVisibleItemCount(3);
		
		DatePicker datePicker = new DatePicker();
		datePicker.addValueChangeHandler(new ValueChangeHandler<Date>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				Date date = event.getValue();
				String dateString = DateTimeFormat.getMediumDateFormat().format(date);
				setDate.setText(dateString);
				
			}
		});
		datePicker.setValue(new Date(), true);
	}



}
