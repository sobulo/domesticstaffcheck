package com.fertiletech.domestichelp.client.gui;

public class ApplicantsInformation {
	
	private String title = "Mr";
	private String firstName = "Wale";
	private String lastName = "Bakare";
	private String dob = "12/12/1984";
	private String address = "3B Oliade Benson Street, Maryland";
	private String email = "bakare@fertiletech.com";
	private String skill = "Cook";
	private String experience = "Have a masters in cookery";
	private String comment = "I take my job very seriously";
	
	private String issue = "theft";
	private String describe = "Was caught stealing meat from the pot of soup";
	private String resolve = "Case is been investigated by the Police";
	
	public String getResolve() {
		return resolve;
	}
	
	public String getDescription() {
		return describe;
	}
	
	public String getIssue() {
		return issue;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getDob() {
		return dob;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getSkill() {
		return skill;
	}
	
	public String getExperience() {
		return experience;
	}
	
	public String getComment() {
		return comment;
	}

}
