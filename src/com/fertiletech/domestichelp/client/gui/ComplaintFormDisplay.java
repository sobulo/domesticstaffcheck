package com.fertiletech.domestichelp.client.gui;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DatePicker;

public class ComplaintFormDisplay extends Composite {
	
	@UiField
	Label applicantLabel;
	
	@UiField
	Label issueLabel;
	
	@UiField
	Label describeLabel;
	
	@UiField
	Label setDate;
	
	@UiField
	Label resolveLabel;
	
	ApplicantsInformation info = new ApplicantsInformation();

	private static ComplaintFormDisplayUiBinder uiBinder = GWT
			.create(ComplaintFormDisplayUiBinder.class);

	interface ComplaintFormDisplayUiBinder extends
			UiBinder<Widget, ComplaintFormDisplay> {
	}

	public ComplaintFormDisplay() {
		initWidget(uiBinder.createAndBindUi(this));
		
		applicantLabel.setText(info.getFirstName());
		issueLabel.setText(info.getIssue());
		describeLabel.setText(info.getDescription());
		DatePicker datePicker = new DatePicker();
		datePicker.addValueChangeHandler(new ValueChangeHandler<Date>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				Date date = event.getValue();
				String dateString = DateTimeFormat.getMediumDateFormat().format(date);
				setDate.setText(dateString);
				
			}
		});
		datePicker.setValue(new Date(), true);
		
		resolveLabel.setText(info.getResolve());
	}
	



}
