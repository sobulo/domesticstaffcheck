package com.fertiletech.domestichelp.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class FrontEndPanel extends Composite implements ClickHandler {
	
	@UiField
	ListBox titleList;
	
	@UiField
	TextBox firstName;
	
	@UiField
	TextBox surName;
	
	@UiField
	TextBox dob;
	
	@UiField
	TextArea address;
	
	@UiField
	TextArea experience;
	
	@UiField
	TextArea comments;
	
	@UiField
	TextBox eMail;
	
	@UiField
	TextBox other;
	
	@UiField
	CheckBox skillDriver;
	
	@UiField
	CheckBox skillNanny;
	
	@UiField
	CheckBox skillGardner;
	
	@UiField
	CheckBox skillCook;
	
	@UiField
	CheckBox skillOther;
	
	@UiField
	CheckBox skillCleaner;
	
	@UiField
	VerticalPanel imageUpload;
	
	@UiField
	Button submit;
	

	private static FrontEndPanelUiBinder uiBinder = GWT
			.create(FrontEndPanelUiBinder.class);

	interface FrontEndPanelUiBinder extends UiBinder<Widget, FrontEndPanel> {
	}

	public FrontEndPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		
		titleList.addItem("Mr");
		titleList.addItem("Mrs");
		titleList.addItem("Ms");
		titleList.addItem("Miss");
		
		skillDriver.setText("Driver");
		skillNanny.setText("Nanny");
		skillGardner.setText("Garderner");
		skillCook.setText("Cook");
		skillCleaner.setText("Cleaner");
		skillOther.setText("Other");
		
		skillOther.addClickHandler(this);
		other.setEnabled(false);
		
		FileUpload upload = new FileUpload();
		upload.setName("upload");
		imageUpload.add(upload);
		imageUpload.setHorizontalAlignment(HasAlignment.ALIGN_RIGHT);
	}

	@Override
	public void onClick(ClickEvent event) {
		
		other.setEnabled(!other.isEnabled());
		
	}


	

	

}
