package com.fertiletech.domestichelp.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class RegistrationPresentation extends Composite {
	
	@UiField
	Label titleLabel;
	
	@UiField
	Image imageField;
	
	@UiField
	Label firstNameLabel;
	
	@UiField
	Label lastNameLabel;
	
	@UiField
	Label dobLabel;
	
	@UiField
	Label addressLabel;
	
	@UiField
	Label emailLabel;
	
	@UiField
	Label skillLabel;
	
	@UiField
	Label experienceLabel;
	
	@UiField
	Label commentLabel;
	
	ApplicantsInformation info = new ApplicantsInformation();

	private static RegistrationPresentationUiBinder uiBinder = GWT
			.create(RegistrationPresentationUiBinder.class);

	interface RegistrationPresentationUiBinder extends
			UiBinder<Widget, RegistrationPresentation> {
	}

	public RegistrationPresentation() {
		initWidget(uiBinder.createAndBindUi(this));
		
		titleLabel.setText(info.getTitle());
		imageField.setUrl("https://sites.google.com/a/fertiletech.com/household-safety-check/Chuboy.jpg");
		imageField.setTitle("Insert Image Here");
		firstNameLabel.setText(info.getFirstName());
		lastNameLabel.setText(info.getLastName());
		dobLabel.setText(info.getDob());
		addressLabel.setText(info.getAddress());
		emailLabel.setText(info.getEmail());
		skillLabel.setText(info.getSkill());
		experienceLabel.setText(info.getExperience());
		commentLabel.setText(info.getComment());
	}



}
