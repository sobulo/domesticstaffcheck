package com.fertiletech.domestichelp.client;

import com.fertiletech.domestichelp.client.gui.ComplaintForm;
import com.fertiletech.domestichelp.client.gui.ComplaintFormDisplay;
import com.fertiletech.domestichelp.client.gui.FrontEndPanel;
import com.fertiletech.domestichelp.client.gui.RegistrationPresentation;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Datacaptureforworkers implements EntryPoint {
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		
		/*FrontEndPanel 
		
		Panel rootPanel = RootPanel.get("contentArea");
		FrontEndPanel contentPanel = null;
		contentPanel = new FrontEndPanel();
		rootPanel.getElement().setInnerHTML(""); // add(panel);
		rootPanel.add(contentPanel);
	
		
		 Complaint Form
		
		Panel rootPanel = RootPanel.get("contentArea");
		ComplaintForm contentPanel = null;
		contentPanel = new ComplaintForm();
		rootPanel.getElement().setInnerHTML(""); // add(panel);
		rootPanel.add(contentPanel);
		*/
		
		/* Registration Presentation  */
		
		Panel rootPanel = RootPanel.get("contentArea");
		RegistrationPresentation contentPanel = null;
		contentPanel = new RegistrationPresentation();
		rootPanel.getElement().setInnerHTML(""); // add(panel);
		rootPanel.add(contentPanel);
		
		/*
		Complaints Screen
		Panel rootPanel = RootPanel.get("contentArea");
		ComplaintFormDisplay contentPanel = null;
		contentPanel = new ComplaintFormDisplay();
		rootPanel.getElement().setInnerHTML(""); // add(panel);
		rootPanel.add(contentPanel);
		*/
	}
	
}
